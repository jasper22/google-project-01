module github.com/jasper22/google-project-01/backend/server

go 1.12

require (
	github.com/gin-contrib/static v0.0.0-20190913125243-df30d4057ba1
	github.com/gin-gonic/gin v1.4.0
	golang.org/x/tools/gopls v0.1.7 // indirect
)
