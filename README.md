# GoogleProject01

GO based server that serve Angular application

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[![GitHub license](https://img.shields.io/github/license/jasper22/google-project-01?style=plastic)](https://github.com/jasper22/google-project-01/blob/master/LICENSE)
[![Languages used](https://img.shields.io/github/languages/count/jasper22/google-project-01?style=plastic)]
[![GitHub issues](https://img.shields.io/github/issues/Naereen/StrapDown.js.svg)](https://github.com/jasper22/google-project-01/issues/)
[![Open Source Love svg1](https://badges.frapsoft.com/os/v1/open-source.svg?v=103)](https://github.com/jasper22/google-project-01/)

## Getting Started

TODO
### Prerequisites

TODO

```
TODO
```

### Installing

TODO

```
TODO
```


TODO

## Running the tests

TODO

### Break down into end to end tests

TODO

### And coding style tests

TODO

## Deployment

TODO

## Built With

* [GO](https://golang.org/)
* [Angular](https://angular.io/)
* [Project layout](https://github.com/golang-standards/project-layout)

## Contributing

TODO

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/jasper22/google-project-01/tags). 

## Authors

TOTO

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

TODO